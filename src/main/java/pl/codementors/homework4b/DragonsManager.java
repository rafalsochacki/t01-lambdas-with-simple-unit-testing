package pl.codementors.homework4b;

import pl.codementors.homework4b.model.Dragon;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Rafał Sochacki
 */
public class DragonsManager {
    /**
     * Creating set to put dragons in.
     */
    public final Set<Dragon> dragonsList = new HashSet<>();

    /**
     * @param colour to put a colour which want to be checked by.
     * @return boolean value:
     * true - if atleast one dragon is in colour.
     * false - if there is none dragon passing the parameter
     */
    public boolean atleastOneInColour(String colour) {
        return dragonsList.stream().anyMatch(dragon -> dragon.getColour().equals(colour));
    }

    /**
     * @param ageLimit to put an age limiter to dragons.
     * @return boolean values:
     * true - if all dragons are older than the limit.
     * false - if all dragons are younger than the limit.
     */
    public boolean higherAgeThan(int ageLimit) {
        return dragonsList.stream().allMatch(dragon -> dragon.getAge() > ageLimit);
    }

    /**
     * @return a new list of dragons colours in upper case letters..
     */
    public List<String> newListForColursUp() {
        return dragonsList.stream().map(dragon -> dragon.getColour().toUpperCase()).collect(Collectors.toList());
    }

    /**
     * @return a new list of dragons names only.
     */
    public List<String> newListForNamesOnly() {
        return dragonsList.stream().map(Dragon::getName).collect(Collectors.toList());
    }

    /**
     * @param colour to put a colour value.
     * @return a new list of dragons sorted by their colours.
     */
    public List<Dragon> newListByColour(String colour) {
        return dragonsList.stream().filter(dragon -> dragon.getColour().equals(colour)).collect(Collectors.toList());
    }

    /**
     * @return a new list of dragons sorted by their age.
     */
    public List<Dragon> newListByAge() {
        return dragonsList.stream().sorted(Comparator.comparing(Dragon::getAge)).collect(Collectors.toList());
    }

    /**
     * @return a single dragon with longest name.
     */
    public Dragon longestName() {
        return dragonsList.stream().max(Comparator.comparing(Dragon::getNameLength)).get();
    }

    /**
     * @return a single dragon with longest wingspan.
     */
    public Dragon longestWingspan() {
        return dragonsList.stream().max(Comparator.comparing(Dragon::getWingspan)).get();
    }

    /**
     * @return a single dragon with highest age.
     */
    public Dragon highestAge() {
        return dragonsList.stream().max(Comparator.comparing(Dragon::getAge)).get();
    }

    /**
     * @param name of the dragon to remove.
     */
    public boolean removeDragon(String name) {
        return dragonsList.removeIf(dragon -> dragon.getName().equals(name));
    }

    /**
     * @return set(hashSet) of dragons
     */
    public Set<Dragon> getDragons() {
        return Collections.unmodifiableSet(dragonsList);
    }

    /**
     * @param dragon to add dragon to the list.
     */
    public void addDragon(Dragon dragon) {
        dragonsList.add(dragon);
    }
}
