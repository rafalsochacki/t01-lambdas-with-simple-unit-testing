package pl.codementors.homework4b.model;

import java.util.Objects;

/**
 * Single Dragon representation.
 *
 * @author Rafał Sochacki
 */

public class Dragon implements Comparable<Dragon> {
    /**
     * Dragon name.
     */
    private String name;
    /**
     * Dragon age.
     */
    private int age;
    /**
     * Dragon colour.
     */
    private String colour;
    /**
     * Dragon wingspan.
     */
    private int wingspan;

    /**
     * Single dragon constructour without parameters
     */
    public Dragon() {
    }

    /**
     * @param name     to set dragon name.
     * @param age      to set dragon age.
     * @param colour   to set dragon colour.
     * @param wingspan to set dragon wingspan.
     */
    public Dragon(String name, int age, String colour, int wingspan) {
        this.name = name;
        this.age = age;
        this.colour = colour;
        this.wingspan = wingspan;
    }

    public String getName() {
        return name;
    }

    public int getNameLength() {
        return name.length();
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public int getWingspan() {
        return wingspan;
    }

    public void setWingspan(int wingspan) {
        this.wingspan = wingspan;
    }

    /**
     * @param o to check if one dragon is equal to another dragon.
     * @return true of false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dragon dragon = (Dragon) o;
        return age == dragon.age &&
                wingspan == dragon.wingspan &&
                Objects.equals(name, dragon.name) &&
                Objects.equals(colour, dragon.colour);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, age, colour, wingspan);
    }

    /**
     * @return in string parameters of 1 dragon
     */
    @Override
    public String toString() {
        return "Dragon{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", colour='" + colour + '\'' +
                ", wingspan=" + wingspan +
                '}';
    }

    /**
     * @param other to compare 1 dragon to another one.
     * @return int value if:
     * int < 0 then comparing dragon is "smaller" than dragon to compare with.
     * int = 0 then comparing dragon has the same parameters as dragon to compare with.
     * int > 0 then comparing dragon is "bigger" than dragon to campare with.
     */
    @Override
    public int compareTo(Dragon other) {

        if (name == null) {
            return 1;
        }
        int ret = name.compareTo(other.name);
        if (ret == 0) {
            ret = age - other.age;
        }
        if (ret == 0) {
            ret = colour.compareTo(other.colour);
        }
        if (ret == 0) {
            ret = wingspan - other.wingspan;
        }
        return ret;
    }
}
