package pl.codementors.homework4b;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;
import pl.codementors.homework4b.model.Dragon;

import java.util.List;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * @author Rafał Sochacki.
 */

@RunWith(PowerMockRunner.class) //using Power Mockito
public class DragonsManagerTest {

    private DragonsManager manager;
    private Dragon dragon1, dragon2, dragon3, dragon4;

    @Rule
    public ExpectedException exceptionGrabber = ExpectedException.none();

    @Before
    public void prepare() {
        manager = new DragonsManager();
        dragon1 = new Dragon("Mirek", 200, "blue", 1000);
        dragon2 = new Dragon("Wojtek", 250, "red", 600);
        dragon3 = new Dragon("Mariano", 150, "green", 800);
        dragon4 = new Dragon("Toudi", 100, "black", 50);
    }

    @Test
    public void DragonWhenCreated() {
        assertNotNull("collection should exist", manager.getDragons());
        assertThat("Collection should be empty", manager.getDragons().isEmpty(), is(true));
    }

    @Test
    public void addWhenParamProvided() {
        manager.addDragon(dragon1);
        assertThat(manager.getDragons().size(), is(1));
        assertThat(manager.getDragons(), hasItem(dragon1));
    }

    @Test
    public void highestAgeWhenFiltered() {
        manager.addDragon(dragon1);
        manager.addDragon(dragon2);
        manager.addDragon(dragon3);
        manager.addDragon(dragon4);

        Dragon dragon = manager.highestAge();

        Assert.assertEquals("Should be highest age value", 250, dragon.getAge());
    }

    @Test
    public void longestWingspanWhenFiltered() {
        manager.addDragon(dragon1);
        manager.addDragon(dragon2);
        manager.addDragon(dragon3);
        manager.addDragon(dragon4);

        Dragon dragon = manager.longestWingspan();

        Assert.assertEquals("Should be longest wingspan value", 1000, dragon.getWingspan());
    }

    @Test
    public void longestNameWhenFiltered() {
        manager.addDragon(dragon1);
        manager.addDragon(dragon2);
        manager.addDragon(dragon3);
        manager.addDragon(dragon4);

        Dragon dragon = manager.longestName();

        Assert.assertEquals("Should be longest name value", 7, dragon.getName().length());
    }

    @Test
    public void newListFilteredByAge() {
        manager.addDragon(dragon1);
        manager.addDragon(dragon2);
        manager.addDragon(dragon3);
        manager.addDragon(dragon4);

        List<Dragon> list = manager.newListByAge();

        Assert.assertEquals("Should have 4 Dragons", 4, list.size());
    }

    @Test
    public void newListFilteredByColour() {
        manager.addDragon(dragon1);
        manager.addDragon(dragon2);
        manager.addDragon(dragon3);
        manager.addDragon(dragon4);

        List<Dragon> list = manager.newListByColour("red");

        Assert.assertEquals("Should have 1 Dragon", 1, list.size());
    }

    @Test
    public void newListForNamesOnlyAfterMaping() {
        manager.addDragon(dragon1);
        manager.addDragon(dragon2);
        manager.addDragon(dragon3);
        manager.addDragon(dragon4);

        List<String> list = manager.newListForNamesOnly();

        Assert.assertEquals("Should have 4 Dragons", 4, list.size());
    }

    @Test
    public void newListForColursUpperCaseAfterMaping() {
        manager.addDragon(dragon1);
        manager.addDragon(dragon2);
        manager.addDragon(dragon3);
        manager.addDragon(dragon4);

        List<String> list = manager.newListForColursUp();

        Assert.assertEquals("Should have 4 Dragons", 4, list.size());
    }

    @Test
    public void CheckAllDragonsByAgeValue() {
        manager.addDragon(dragon1);
        manager.addDragon(dragon2);
        manager.addDragon(dragon3);
        manager.addDragon(dragon4);

        manager.higherAgeThan(10);

        Assert.assertTrue("should be true", true);
    }

    @Test
    public void CheckIfAtleastOneDragonIsInColour() {
        manager.addDragon(dragon1);
        manager.addDragon(dragon2);
        manager.addDragon(dragon3);
        manager.addDragon(dragon4);

        manager.atleastOneInColour("red");

        Assert.assertTrue("should be true", true);
    }

    @Test
    public void CheckIfAtleastOneDragonIsInColourWhenNumberProvided() {
        manager.addDragon(dragon1);
        manager.addDragon(dragon2);
        manager.addDragon(dragon3);
        manager.addDragon(dragon4);

        manager.atleastOneInColour(String.valueOf(1));

        Assert.assertFalse("should be false", false);
    }

    @Test
    public void removeDragonTest() {
        manager.addDragon(dragon1);
        manager.addDragon(dragon2);
        manager.addDragon(dragon3);
        manager.addDragon(dragon4);

        manager.removeDragon("Mirek");

        Assert.assertFalse("Dragon should be deleted", manager.dragonsList.contains("Mirek"));
    }
}
