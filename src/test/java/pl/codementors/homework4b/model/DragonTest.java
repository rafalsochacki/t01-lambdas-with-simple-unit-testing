package pl.codementors.homework4b.model;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Rafał Sochacki
 */

public class DragonTest {

    /**
     * test to check if all data put into constructor is correct.
     */
    @Test
    public void DragonWhenAllDataProvided() {
        String name = "Dobromir";
        int age = 200;
        String colour = "black";
        int wingspan = 300;

        Dragon dragon = new Dragon(name, age, colour, wingspan);

        Assert.assertEquals("Dragon name not found", name, dragon.getName());
        Assert.assertEquals("Dragon age not found", age, dragon.getAge());
        Assert.assertEquals("Dragon colour not found", colour, dragon.getColour());
        Assert.assertEquals("Dragon wingspan not found", wingspan, dragon.getWingspan());
    }

    /**
     * test to check if created dragon has access to all data in it.
     */
    @Test
    public void DragonWithNoParametersProvided() {
        Dragon dragon = new Dragon();

        Assert.assertNull(dragon.getName());
        Assert.assertEquals(0, dragon.getAge());
        Assert.assertNull(dragon.getColour());
        Assert.assertEquals(0, dragon.getWingspan());
    }

    /**
     * test to check if name will be set properly.
     */
    @Test
    public void setName() {
        Dragon dragon = new Dragon();
        dragon.setName("Dobromir");

        Assert.assertEquals("Name not set properly", "Dobromir", dragon.getName());
    }

    /**
     * test to check if age will be set properly.
     */
    @Test
    public void setAge() {
        Dragon dragon = new Dragon();
        dragon.setAge(150);

        Assert.assertEquals("Age not set properly", 150, dragon.getAge());
    }

    /**
     * test to check if colour will be set properly.
     */
    @Test
    public void setColour() {
        Dragon dragon = new Dragon();
        dragon.setColour("Blue");

        Assert.assertEquals("Colour not set properly", "Blue", dragon.getColour());
    }

    /**
     * test to check if colour will be set properly.
     */
    @Test
    public void setWingspan() {
        Dragon dragon = new Dragon();
        dragon.setWingspan(200);

        Assert.assertEquals("Wingspan not set properly", 200, dragon.getWingspan());
    }

    /**
     * test to check if references are the same.
     */
    @Test
    public void equalsWhenTheSameReference() {
        Dragon d1 = new Dragon();
        Dragon d2 = d1;
        boolean ret = d1.equals(d2);
        Assert.assertTrue("objects should be the same", ret);
    }

    /**
     * test to check if dragons are equal by different references but same values.
     */
    @Test
    public void equalsWhenDifferentReferenceSameValues() {
        Dragon d1 = new Dragon("Mirek", 200, "blue", 1000);
        Dragon d2 = new Dragon("Mirek", 200, "blue", 1000);
        boolean ret = d1.equals(d2);
        Assert.assertTrue("objects should be the same", ret);
    }

    /**
     * test to check if dragons are equal by different references and different values.
     */
    @Test
    public void equalsWHenDifferentReferenceDifferentValues() {
        Dragon d1 = new Dragon("Mirek", 200, "blue", 1000);
        Dragon d2 = new Dragon("Wojtek", 250, "red", 600);
        boolean ret = d1.equals(d2);
        Assert.assertFalse("objects should not be the same", ret);
    }

    /**
     * test to check if dragons have same hashcode while same values.
     */
    @Test
    public void hashCodeWhenSameValue() {
        Dragon d1 = new Dragon("Mirek", 200, "blue", 1000);
        Dragon d2 = new Dragon("Mirek", 200, "blue", 1000);
        int hash1 = d1.hashCode();
        int hash2 = d2.hashCode();
        Assert.assertTrue("hash codes should be the same", hash1 == hash2);
    }

    /**
     * test to check if object dragon can be printed out on standard output in String format.
     */
    @Test
    public void toStringWhenAllParametersSet() {
        Dragon d1 = new Dragon("Mirek", 200, "blue", 1000);
        String toString = d1.toString();
        Assert.assertEquals("string wrong formatter", "Dragon{name='Mirek', age=200, colour='blue', wingspan=1000}", toString);
    }

    /**
     * test to compare dragons with same parameters.
     */
    @Test
    public void comparingDragonsWithSameParameters() {
        Dragon d1 = new Dragon("Mirek", 200, "blue", 1000);
        Dragon d2 = new Dragon("Mirek", 200, "blue", 1000);
        int ret = d1.compareTo(d2);
        Assert.assertEquals("Object should be the same", ret = 0, d1.compareTo(d2));
    }

    /**
     * test to compare dragons with different parameters.
     */
    @Test
    public void comparingDragonsWithDifferentParameters() {
        Dragon d1 = new Dragon("Mirek", 200, "blue", 1000);
        Dragon d2 = new Dragon("Wojtek", 250, "red", 600);
        int ret = d1.compareTo(d2);
        Assert.assertNotEquals("Object should be different", ret != 0, d1.compareTo(d2));
    }

}
